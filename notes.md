﻿# Notes

**Backend:**

_Explain all choices made and why you might do this in a real project or maybe
you just wanted to play with something new._

I followed Django tutorials to figure out how to store the name of the company
in a database, and then make the api viewable at
http://localhost:8000/api/company. I chose to consume the API using jQuery as
this would facilitate the project’s simple API request. I have rarely used
jQuery but as it is popular, I figured it would be a good chance to do a
refresher. I usually use React.js when dealing with Javascript but there would
be no point in this implementation due to the site’s lack of complexity.

Note: the client can change the company name by visiting
http://localhost:8000/admin/company/company/ while Docker is running. You can
log in with username: username and password: username123 which I would give to
the client. I made it check if there is already a name so it would produce an
error if you tried to create another name.

**Devops:**

_After doing this briefly explain what you like and/or dislike about using
docker._

As the instructions stated, using Docker simplified what I would normally need
to do with a virtual machine; I just needed to type “docker-compose up” to get
the site to run. This greatly expedites a typically time-consuming process where
you want to create and run your applications. Since Docker is open source, it is
easy to use on all kinds of operating systems as there is widespread support. It
also allows programs to run the same, independently of their platforms.

One of the difficulties I had with Docker is that since I am not familiar with
Django/Python, I was heavily reliant on following other individuals’
instructions, blogs, and youtube videos. As none of the tutorials I found used
Docker, I frequently was told to use commands that would not work for me as I
did not at first realize I had to write “sudo docker-compose run web” before the
rest of my command. In a more general sense, Docker could be problematic due to
it being less secure than virtual machines.

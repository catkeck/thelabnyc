from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Company
from .serializers import companySerializer


def home(request):
    name = Company.objects.all()[:1].get()
    context = {
        'name': name
    }
    return render(
        request,
        'company/home.html',
        context
    )


class companyList(APIView):
    def get(self, request):
        company1 = Company.objects.all()
        serializer = companySerializer(company1, many=True)
        return Response(serializer.data)

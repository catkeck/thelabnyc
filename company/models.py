# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError


class Company(models.Model):
    name = models.CharField(default="Company Name", max_length=100)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if Company.objects.exists() and not self.pk:
            raise ValidationError('There is only one Company')
        return super(Company, self).save(*args, **kwargs)

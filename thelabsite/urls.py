from django.conf.urls import include, url
from django.core.urlresolvers import RegexURLResolver
from django.contrib import admin
from company import views


def lookup_root(urlconf):
    urlconf_module, app_name, namespace = include(urlconf)
    resolver = RegexURLResolver(
        '^', urlconf_module, app_name=app_name, namespace=namespace)
    return resolver.resolve('').func


urlpatterns = [
    url(r'^$', lookup_root('company.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^company/', include('company.urls')),
    url(r'^api/company', views.companyList.as_view())
]
